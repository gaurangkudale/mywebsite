The error message `unknown option '--output-dir'` indicates that the `docusaurus build` command does not recognize the `--output-dir` option. Instead, you should use the correct option, which is `--out-dir`.

### Correcting the Build Command

To specify a custom output directory for the Docusaurus build, you should use the `--out-dir` option correctly.

Here’s how you should run the build command:

```bash
yarn build --out-dir ../public
```

### Updated `package.json` Script

If you have a custom script in your `package.json` file to handle this build process, ensure it uses the correct `--out-dir` option. For example:

```json
"scripts": {
  "build": "docusaurus build",
  "build:gitlab": "docusaurus build --out-dir ../public"
}
```

### Complete Workflow

1. **Update `package.json`**: Ensure your `package.json` scripts are correctly defined.

   ```json
   "scripts": {
     "build": "docusaurus build",
     "build:gitlab": "docusaurus build --out-dir ../public"
   }
   ```

2. **Run the Build Command**: Execute the build script using Yarn.

   ```bash
   yarn build:gitlab
   ```

This command will build your Docusaurus site and output the files to the `../public` directory as specified.

### Additional Considerations

- **Check Version Compatibility**: Ensure that your Docusaurus version supports the options you are using. You can check the available options by running `docusaurus build --help`.

- **Output Directory Path**: Ensure that the specified output directory path (`../public`) is correct relative to your project root.

### Example Directory Structure

Assuming your project directory structure is as follows:

```
my-website/
├── website/
│   ├── docusaurus.config.js
│   ├── package.json
│   └── ...
└── public/
```

In this structure, running the command `docusaurus build --out-dir ../public` from the `website` directory will place the built files in the `public` directory adjacent to `website`.

By following these steps, you should be able to successfully build your Docusaurus site with the output directed to your specified directory without encountering the `unknown option '--output-dir'` error.