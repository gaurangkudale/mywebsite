import React from 'react';
import ComponentCreator from '@docusaurus/ComponentCreator';

export default [
  {
    path: '/my-website/ja/blog',
    component: ComponentCreator('/my-website/ja/blog', 'f76'),
    exact: true
  },
  {
    path: '/my-website/ja/blog/archive',
    component: ComponentCreator('/my-website/ja/blog/archive', '84c'),
    exact: true
  },
  {
    path: '/my-website/ja/blog/first-blog-post',
    component: ComponentCreator('/my-website/ja/blog/first-blog-post', 'd11'),
    exact: true
  },
  {
    path: '/my-website/ja/blog/long-blog-post',
    component: ComponentCreator('/my-website/ja/blog/long-blog-post', '98c'),
    exact: true
  },
  {
    path: '/my-website/ja/blog/mdx-blog-post',
    component: ComponentCreator('/my-website/ja/blog/mdx-blog-post', '0e2'),
    exact: true
  },
  {
    path: '/my-website/ja/blog/tags',
    component: ComponentCreator('/my-website/ja/blog/tags', 'd35'),
    exact: true
  },
  {
    path: '/my-website/ja/blog/tags/docusaurus',
    component: ComponentCreator('/my-website/ja/blog/tags/docusaurus', 'bce'),
    exact: true
  },
  {
    path: '/my-website/ja/blog/tags/facebook',
    component: ComponentCreator('/my-website/ja/blog/tags/facebook', '26a'),
    exact: true
  },
  {
    path: '/my-website/ja/blog/tags/hello',
    component: ComponentCreator('/my-website/ja/blog/tags/hello', '0f3'),
    exact: true
  },
  {
    path: '/my-website/ja/blog/tags/hola',
    component: ComponentCreator('/my-website/ja/blog/tags/hola', 'dd5'),
    exact: true
  },
  {
    path: '/my-website/ja/blog/welcome',
    component: ComponentCreator('/my-website/ja/blog/welcome', '0a4'),
    exact: true
  },
  {
    path: '/my-website/ja/markdown-page',
    component: ComponentCreator('/my-website/ja/markdown-page', 'c5b'),
    exact: true
  },
  {
    path: '/my-website/ja/docs',
    component: ComponentCreator('/my-website/ja/docs', '076'),
    routes: [
      {
        path: '/my-website/ja/docs',
        component: ComponentCreator('/my-website/ja/docs', '061'),
        routes: [
          {
            path: '/my-website/ja/docs',
            component: ComponentCreator('/my-website/ja/docs', 'c53'),
            routes: [
              {
                path: '/my-website/ja/docs/category/tutorial---basics',
                component: ComponentCreator('/my-website/ja/docs/category/tutorial---basics', '3d7'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/my-website/ja/docs/category/tutorial---extras',
                component: ComponentCreator('/my-website/ja/docs/category/tutorial---extras', 'd3a'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/my-website/ja/docs/intro',
                component: ComponentCreator('/my-website/ja/docs/intro', '516'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/my-website/ja/docs/tutorial-basics/congratulations',
                component: ComponentCreator('/my-website/ja/docs/tutorial-basics/congratulations', '7dd'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/my-website/ja/docs/tutorial-basics/create-a-blog-post',
                component: ComponentCreator('/my-website/ja/docs/tutorial-basics/create-a-blog-post', 'f9a'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/my-website/ja/docs/tutorial-basics/create-a-document',
                component: ComponentCreator('/my-website/ja/docs/tutorial-basics/create-a-document', '518'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/my-website/ja/docs/tutorial-basics/create-a-page',
                component: ComponentCreator('/my-website/ja/docs/tutorial-basics/create-a-page', 'cc0'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/my-website/ja/docs/tutorial-basics/deploy-your-site',
                component: ComponentCreator('/my-website/ja/docs/tutorial-basics/deploy-your-site', 'cc4'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/my-website/ja/docs/tutorial-basics/markdown-features',
                component: ComponentCreator('/my-website/ja/docs/tutorial-basics/markdown-features', 'f09'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/my-website/ja/docs/tutorial-extras/manage-docs-versions',
                component: ComponentCreator('/my-website/ja/docs/tutorial-extras/manage-docs-versions', '08b'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/my-website/ja/docs/tutorial-extras/translate-your-site',
                component: ComponentCreator('/my-website/ja/docs/tutorial-extras/translate-your-site', '78d'),
                exact: true,
                sidebar: "tutorialSidebar"
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: '/my-website/ja/',
    component: ComponentCreator('/my-website/ja/', '85b'),
    exact: true
  },
  {
    path: '*',
    component: ComponentCreator('*'),
  },
];
