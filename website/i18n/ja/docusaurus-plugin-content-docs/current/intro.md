---
sidebar_position: 1
---

# チュートリアル紹介

**Docusaurusを5分以内で発見しましょう**。

## はじめに

**新しいサイトを作成**することから始めましょう。 format@@0

もしくは、\*\*[docusaurus.new](https://docusaurus.new)\*\*を使用して、直ちにDocusaurusを試してみてください。

### 必要なもの

- [Node.js](https://nodejs.org/en/download/) version 18.0 以上:
  - Node.js をインストールする場合、依存関係に関連するすべてのチェックボックスをチェックすることをお勧めします。

## 新しいサイトを生成

**古典的なテンプレート**を使用して新しいDocusaurusサイトを生成します。

コマンドを実行すると、古典的なテンプレートがプロジェクトに自動的に追加されます。

```bash
npm init docusaurus@latest my-website classic
```

このコマンドは、コマンドプロンプト、Powershell、ターミナル、またはコードエディタの他の統合端子に入力できます。

このコマンドは、Docusaurusを実行するために必要な依存関係をすべてインストールします。

## サイトを開始

開発サーバーの実行:

```bash
cd my-website
npm run start
```

`cd`コマンドは、作業中のディレクトリを変更します。 新しく作成したDocusaurusサイトを操作するには、ターミナルをナビゲートする必要があります。

`npm run start` コマンドはあなたのウェブサイトをローカルにビルドし、開発サーバーを通してそれを提供します。http://localhost:3000/ で表示できます。

`docs/intro.md` (このページ) を開き、いくつかの行を編集します: サイトは **自動的にリロード** し、変更を表示します。
